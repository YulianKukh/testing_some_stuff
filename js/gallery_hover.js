$(document).ready(function(){
    $(".img-div1, .fi1").mouseenter(function(){
        $('.img-div1').addClass('hover-stuff'); 
        $('.fi1').css('opacity','1');
    });
    $(".img-div2, .fi2").mouseenter(function(){
        $('.img-div2').addClass('hover-stuff');
        $('.fi2').css('opacity','1');
    });
    $(".img-div3, .fi3").mouseenter(function(){
        $('.img-div3').addClass('hover-stuff'); 
        $('.fi3').css('opacity','1');
    });
    $(".img-div1, .fi1").mouseleave(function(){
        $('.img-div1').removeClass('hover-stuff'); 
        $('.fi1').css('opacity','0');
    });
    $(".img-div2, .fi2").mouseleave(function(){
        $('.img-div2').removeClass('hover-stuff'); 
        $('.fi2').css('opacity','0');
    });
    $(".img-div3, .fi3").mouseleave(function(){
        $('.img-div3').removeClass('hover-stuff');
        $('.fi3').css('opacity','0');
    });
});