function render (src) {
	return "<div class='container fullhouse close1'>" +
				 "<div class='row'>" + 
				 	"<div class='col-md-12 kartinkaPOcentri'>" +
				 		"<img class='img-responsive wemenwe' src='" + src + "'/>" + 				 	
				 		"<span class='close1 glyphicon glyphicon-remove-circle' aria-hidden='true'></span>" + 
				 	"</div>"+				 	
				"</div>" +
			"</div>";
}

$(document).ready(function() {
	$('.img-div').click(function(){

		var src = $(this).attr("src");

		$('body').append(render(src)).addClass('stuck_body');		
		$('.navbar').addClass('nav_bar_z_index_reduce');
		$('.fullhouse').click(function(e){	
			if ($(e.target).hasClass('close1')) {
				$('.fullhouse').remove();
				$('body').removeClass('stuck_body');				
			}					
		});					
	});		
});

