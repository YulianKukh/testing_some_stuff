$(document).ready(function(){
  $(window).scroll(function() {
    if ($(document).scrollTop() > 10) {
		$('.navbar').css('background-color','#232731');
        $('.navbar_container').css('border-bottom','0px solid rgba(255,255,255,.2)');
		$('.navbar-collapse').css('padding', '25px 0px 25px 0px');
		$('.navbar-header').css('margin', '-10px 0px 0px 0px');
    } else if ($(document).scrollTop() <= 9) {
		$('.navbar').css('background-color','');
   		$('.navbar_container').css('border-bottom','1px solid rgba(255,255,255,.2)');
  		$('.navbar-collapse').css('padding', '35px 0px 35px 0px');
  		$('.navbar-header').css('margin', '');	
    }
  });
});
/*$(function(){
    var lastScrollTop = 0, delta = 5;
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       
       if(Math.abs(lastScrollTop - st) <= delta)
          return;
       
       if (st > lastScrollTop){
           	// $(".navbar").addClass("navbar-fixed-top");
           	$('.navbar').css('background-color','#232731');
           	$('.navbar_container').css('border-bottom','0px solid rgba(255,255,255,.2)');
      		$('.navbar-collapse').css('padding', '25px 0px 25px 0px');
      		$('.navbar-header').css('margin', '-10px 0px 0px 0px');
       } else {
       		// $(".navbar").removeClass("navbar-fixed-top");
       		$('.navbar').css('background-color','');
       		$('.navbar_container').css('border-bottom','1px solid rgba(255,255,255,.2)');
      		$('.navbar-collapse').css('padding', '35px 0px 35px 0px');
      		$('.navbar-header').css('margin', '');	
       }
       lastScrollTop = st;
    });
});*/