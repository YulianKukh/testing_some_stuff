jQuery(document).ready(function($){
		//duration of the top scrolling animation (in ms)
	var scroll_top_duration = 2000,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');	

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
});